package app.com.androidroomdemo.pojo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "word_table")
public class Word {

    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    @ColumnInfo(name = "word")
    private String mWord;

    @NonNull
    private String mmm;

    public Word(@NonNull String mWord, @NonNull String mmm) {
        this.mWord = mWord;
        this.mmm = mmm;
    }

    @NonNull
    public String getmWord() {
        return mWord;
    }

    public void setmWord(@NonNull String mWord) {
        this.mWord = mWord;
    }

    @NonNull
    public String getMmm() {
        return mmm;
    }

    public void setMmm(@NonNull String mmm) {
        this.mmm = mmm;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWord() {
        return this.mWord;
    }
}